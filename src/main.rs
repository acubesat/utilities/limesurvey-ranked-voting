// Poll results
//
// Note: The [Schulze method](https://en.wikipedia.org/wiki/Schulze_method) was used,
// with the ratio variant (The strength of a link is measured by the ratio of its support
// and opposition). See https://docs.rs/tallystick/0.3.1/tallystick/schulze/enum.Variant.html
// for more information on the supported Schulze variants.
//
// There are tons of different ways to calculate ranked results from ranked elections - Schultze
// seems to be the most popular. See https://en.wikipedia.org/wiki/Voting_system_criterion for
// an interesting comparison. Λόγω των πολλών επιλογών και των διαφορετικών απόψεων του καθενός,
// τα αποτελέσματα είναι πολύ volatile :P
//
// [dependencies]
// tallystick = { version = "0.3.1", features=["nightly"] }
// csv = "1.1"
// itertools = "0.9.0"

extern crate tallystick;
extern crate itertools;

#[macro_use]
use itertools::Itertools;

fn main() {
    use tallystick::schulze::SchulzeTally;
    use tallystick::schulze::Variant;
    use tallystick::plurality::DefaultPluralityTally;
    use tallystick::borda::DefaultBordaTally;
    use tallystick::borda::Variant as BordaVariant;
    use std::fs::File;
    use std::rc::Rc;

    // Expect file in arguments
    let pattern = std::env::args().nth(1).expect("Usage: ./program <results.csv>");

    // Read from CSV
    let file = File::open(pattern).expect("Could not open file");
    let mut rdr = csv::Reader::from_reader(file);
    let headers = rdr.headers().expect("File does not contain headers?");

    // Parse headers to find which ones are interesting
    let indexes : Vec<usize> = headers.iter().enumerate()
        .filter(|x| x.1.contains("["))
        .map(|x| x.0)
        .collect();
    println!("{:?}", indexes);

    // Get ballots from CSV
    let mut ballots : Vec<Vec<String>> = Vec::new();

    println!("===============\nBALLOTS\n===============");
    for result in rdr.records() {
        let record = result.unwrap();
        let record = indexes.iter()
            .map(|i| record[*i].to_string())
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>();
        println!("{:?}", record);

        ballots.push(record);
    }

    // Get candidates
    let candidates : Vec<String> = ballots.iter().flat_map(|s| s.iter()).map(|s| (*s).clone()).unique().collect();
    println!("===============\nCANDIDATES\n===============");
    println!("{:?}", candidates);

    let ballots : Vec<Vec<&str>> = ballots.iter().map(|b| b.iter().map(std::ops::Deref::deref).collect()).collect();
    
    // First-past-the-post indicative results
    let mut fptp_tally = DefaultPluralityTally::<&str>::new(5);
    for ballot in ballots.clone() {
        fptp_tally.add(ballot[0]);
    }

    let fptp_totals = fptp_tally.totals();
    println!("===============\nFPTP (invalid) RESULTS\n===============");
    println!("{:?}", fptp_totals);

    // Borda indicative results
    let mut borda_tally = DefaultBordaTally::new(1, BordaVariant::ClassicBorda);
    for ballot in ballots.clone() {
        borda_tally.add(ballot);
    }

    let borda_totals = borda_tally.totals();
    println!("===============\nBORDA RESULTS\n===============");
    for (candidate, num_points) in borda_totals.iter() {
        println!("{} has {} points", candidate, num_points);
     }

    // An election for Judge using floats as the count type.
    let mut tally = SchulzeTally::<&str, f64>::new(1, Variant::Ratio);
    tally.add_candidates(candidates.iter().map(std::ops::Deref::deref).collect());
    for ballot in ballots {
        tally.add(&ballot);
    }
    
    let winners = tally.ranked();
    println!("===============\nRESULTS\n===============");
    println!("{:?}", winners);

    for ((candidate1, candidate2), num_votes) in tally.totals().iter() {
        println!("{} is preferred over {} {} times", candidate1, candidate2, num_votes);
     }
}

